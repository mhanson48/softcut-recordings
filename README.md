
# Softcut Recordings
 Documentation of a Home Studio
 
![home-thin-border](https://user-images.githubusercontent.com/867946/181144111-515a0243-29a7-48e6-90e2-a294a21f1db8.jpg)
 * Documentation of a home music production setup
 * Archive of rendered Cubase projects
 * Technologies: 
    - Angular 15
    - GitLab CI/CD Pipeline
    - AWS S3 / Route53 / CloudFront
    - AWS SAM - Lambda Interactions with Dynamo
    - AWS Cognito - Auth
 

 ##  Dev Environment Setup:
```
npm install
```

```
ng serve
```

## Test Suite (Jest)
```
npm run test
```

## SAM Deploy (genre/audio endpoints)
```
sam build
sam deploy
```

 - https://softcutrecordings.com
