import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router'
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { ControlsModule } from './controls/controls.module';
import { NavigatorModule } from './controls/navigator/navigator.module';
import { MaterialModule } from './material.module';
import { BusyInterceptor } from 'src/utils/http-interceptor';
import { LoaderComponent } from './controls/loader/loader.component';

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent }
]

@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        LoaderComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialModule,
        ControlsModule,
        NavigatorModule,
    ],
    providers: [
        BusyInterceptor,
        { provide: HTTP_INTERCEPTORS, useClass: BusyInterceptor, multi: true }
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
