import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { UploadAudioComponent } from './upload-audio.component';
import { ArchiveService } from '../service/archive.service';
import { MaterialModule } from 'src/app/material.module';
import { MatDialogRef } from '@angular/material/dialog';
import { Genre } from '../models/genere.model';
import { of } from 'rxjs';
import { GenreIdentifier } from '../models/genre-identifier';
import { AudioFile } from '../models/audio-file.model';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('Upload Audio Component', () => {

	let uploadAudioComponent: UploadAudioComponent;
	let fixture: ComponentFixture<UploadAudioComponent>;

	const genre = new Genre(new GenreIdentifier('1'), '1', 0);

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				BrowserModule,
				CommonModule,
				FormsModule,
				MaterialModule,
				NoopAnimationsModule,
				HttpClientTestingModule
			],
			declarations: [
				UploadAudioComponent
			],
			providers: [
				{
					provide: ArchiveService, useValue: {
						genres: [
							genre
						],
						selectedGenre: genre,
						allTracks: [
							new AudioFile('test.mp3', new GenreIdentifier('1'), 0)
						],
						createAudio: jest.fn().mockImplementation(() => of([])),
						getSignedS3Url: jest.fn().mockImplementation(() => of([])),
						putToSignedS3Url: jest.fn().mockImplementation(() => of ()),
						getGenreAndAudio: jest.fn().mockImplementation(() => of([]))
					}
				},
				{ provide: MatDialogRef, useValue: {} },
				{
					provide: MatDialogRef, useValue: {
						close: jest.fn()
					}
				},
			]
		}).compileComponents();

		TestBed.inject(ArchiveService);
		TestBed.inject(HttpTestingController);
		fixture = TestBed.createComponent(UploadAudioComponent);
		uploadAudioComponent = <UploadAudioComponent>fixture.componentInstance;

		fixture.detectChanges();
	}));

	it('will call S3 Upload Service upon file input', () => {
		console.log = jest.fn();
		const blob = new Blob(['someBlobData']);
		const file = new File([blob], 'values.json', {
			type: 'application/JSON',
		})
		const fileEvent = {
			target: {
				value: 'R:\fakepath\somesong.mp3',
				files: [file]
			}
		};

		uploadAudioComponent.selectedGenre = genre;
		uploadAudioComponent.attachMp3(fileEvent);
		fixture.detectChanges();

	});

	it('will resolve and reject file change operations', async(() => {
		const blob = new Blob(['someBlobData']);
		const file = new File([blob], 'values.json', {
			type: 'application/JSON',
		})
		const fileEvent = {
			target: {
				value: 'R:\fakepath\somesong.mp3',
				files: [file]
			}
		};

		uploadAudioComponent.selectedGenre = genre;
		uploadAudioComponent.attachMp3(fileEvent);
	}));

	it('can set the order to 0 when no audio files exist', (() => {
		uploadAudioComponent.audioFiles = [];
		fixture.detectChanges();
		uploadAudioComponent.setAudioOrder();
		expect(uploadAudioComponent.audioOrder).toBe(0);
	}))

	it('will can change the selected genre', (() => {
		uploadAudioComponent.genreChange();
	}));

});