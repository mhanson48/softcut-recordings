import { Component, Input, Self } from '@angular/core';
import { Genre } from '../models/genere.model';
import { ArchiveService } from '../service/archive.service';
import { LoaderService } from 'src/app/controls/loader/loader.service';
import { AudioFile } from '../models/audio-file.model';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'upload-audio',
    templateUrl: './upload-audio.component.html',
    styleUrls: ['./upload-audio.component.scss'],
})
export class UploadAudioComponent {

    @Input() selectedGenre: Genre;

    audioFiles: AudioFile[] = [];
    genres: Genre[] = [];
    audioOrder: number = 0;

    constructor(
        private readonly dialogRef: MatDialogRef<Self>,
        private readonly archiveService: ArchiveService,
        private readonly loaderService: LoaderService,
    ) {
        this.genres = this.archiveService.genres;
        this.audioFiles = this.archiveService.allTracks;
        this.setAudioOrder();
    }

    genreChange(): void {
        this.setAudioOrder();
    }

    public attachMp3(event: any): void {
        if (event.target.value) {
            this.loaderService.show();
            const inputFile = event.target.files[0];
            this.uploadAudio(inputFile);
        };
    }

    public uploadAudio(
        inputFile: File,
    ) {
        const audioFile = new AudioFile(
            inputFile.name,
            this.selectedGenre.getIdentifier(),
            this.audioOrder
        );
        this.archiveService.getSignedS3Url(audioFile).subscribe((urlResponse) => {
            const signedUrl = urlResponse.signedUrl;
            this.archiveService.putToSignedS3Url(signedUrl, inputFile).subscribe(() => {
                this.refreshGenreAndAudio();
                this.dialogRef.close();
            });
        });
    }

    refreshGenreAndAudio(): void {
        this.archiveService.getGenreAndAudio().then(() => {
            this.archiveService.forceRefresh();
        });
    }

    public setAudioOrder(): void {
        setTimeout(() => {
            let genreAudio: AudioFile[] = [];
            if (this.audioFiles && this.selectedGenre) {
                genreAudio = this.audioFiles.filter(af => af.getGenreIdentifier().asString() == this.selectedGenre.getIdentifier().asString());
            }

            if (genreAudio.length == 0) {
                this.audioOrder = 0;
            }
            else {
                const maxOrder = genreAudio.reduce((a, b) => a.getOrder() > b.getOrder() ? a : b).getOrder();
                const nextOrder = maxOrder + 1;
                this.audioOrder = nextOrder;
            }
        }, 1)
    }

}