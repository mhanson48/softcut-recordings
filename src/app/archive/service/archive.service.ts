import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { forkJoin, Observable, Subject } from "rxjs";
import { Genre } from "./../models/genere.model";
import System from "src/utils/system";
import { AudioFile } from "../models/audio-file.model";

@Injectable()
export class ArchiveService {

	allTracks: AudioFile[] = [];
	genres: Genre[] = [];
	selectedGenre: Genre;

	private dataRefreshSubject = new Subject();
	dataRefreshObservable = this.dataRefreshSubject.asObservable();

	constructor(
		private http: HttpClient
	) { }

	async getGenreAndAudio(): Promise<any[]> {
		return new Promise((resolve) => {
			const genreData = this.http.get(System.apiURL + '/genre');
			const audioData = this.http.get(System.apiURL + '/audio');
			forkJoin([
				genreData,
				audioData
			]).subscribe((data) => {
				const genres = Genre.fromJson(data[0]);
				genres.sort((a, b) => a.getOrder() - b.getOrder());
				this.genres = genres;
				if (!this.selectedGenre) {
					this.selectedGenre = this.genres[0];
				}
				else if (this.genres.length > 0) {
					this.selectedGenre = this.genres.filter(g => g.getIdentifier().asString() == this.selectedGenre.getIdentifier().asString())[0];
				}

				const audioFiles = AudioFile.fromJson(data[1]);
				audioFiles.sort((a, b) => a.getOrder() - b.getOrder());
				this.allTracks = audioFiles;
				resolve([
					genres,
					audioFiles
				]);
			})
		});
	}

	createGenre(genre: Genre): Observable<any> {
		const body = {
			genre: genre.getIdentifier().asString(),
			description: genre.getDescription(),
			order: genre.getOrder()
		};
		const headers = {
			headers: { 'Content-Type': 'application/json' }
		};
		return this.http.post<any>(System.apiURL + '/genre', body, headers);
	}

	updateGenre(genre: Genre): Observable<any> {
		const body = {
			genre: genre.getIdentifier().asString,
			description: genre.getDescription(),
			order: genre.getOrder()
		};
		const headers = {
			headers: { 'Content-Type': 'application/json' }
		};
		return this.http.put<any>(System.apiURL + '/genre', body, headers);
	}

	deleteGenre(genre: Genre): Observable<any> {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
			}),
			body: {
				genre: genre.getIdentifier().asString(),
			}
		};
		return this.http.delete<any>(System.apiURL + '/genre', options);
	}

	createAudio(
		audioFile: AudioFile
	): Observable<any> {
		const body = {
			genre: audioFile.getGenreIdentifier().asString(),
			name: audioFile.getFileName(),
			data: audioFile.getAudioBody(),
			order: audioFile.getOrder()
		};
		const headers = {
			headers: { 'Content-Type': 'application/octet-stream' }
		};
		return this.http.post<any>(System.apiURL + '/audio', body, headers);
	}

	deleteAudio(audioFile: AudioFile): Observable<any> {
		const options = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
			}),
			body: {
				name: audioFile.getFileName(),
			}
		};
		return this.http.delete<any>(System.apiURL + '/audio', options);
	}

	getSignedS3Url(audioFile: AudioFile): Observable<any> {
		const body = {
			name: audioFile.getFileName(),
			genre: audioFile.getGenreIdentifier().asString(),
			order: audioFile.getOrder()
		};
		const headers = {
			headers: { 'Content-Type': 'application/json' }
		};
		return this.http.post<any>(System.apiURL + '/audio/signed', body, headers);
	}

	putToSignedS3Url(
		signedUrl: string,
		file: File
	): Observable<any> {
		const headers = {
			headers: {
				'Content-Type': 'audio/mp3',
				'Content-Encoding': 'UTF-8',
			}
		};
		return this.http.put<any>(signedUrl, file, headers);
	}

	updateAudioOrder(audioFiles: AudioFile[]): Observable<any> {
		const body = {
			audioFiles: audioFiles
		};
		const headers = {
			headers: { 'Content-Type': 'application/json' }
		};
		return this.http.put<any>(System.apiURL + '/audio', body, headers);
	}

	public forceRefresh(): void {
		this.dataRefreshSubject.next(true);
	}

}