import { Injectable } from "@angular/core";
import { config, S3 } from "aws-sdk";
import { CognitoService } from "./cognito.service";
import { Genre } from "../models/genere.model";
import { AudioFile } from "../models/audio-file.model";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class S3Service {

	private bucketName: string = 'softcut-assets';

	private updatedSubject = new BehaviorSubject(true);
	updatedObservable = this.updatedSubject.asObservable();

	constructor(
		private cognitoService: CognitoService
	) {
	}

	public listBucketAudio(): Promise<any> {
		return new Promise((resolve, reject) => {
			config.update({
				region: 'us-east-1',
			});
			var s3Params = { Bucket: this.bucketName };
			this.cognitoService.authGuestUser().then(() => {
				const s3 = new S3();
				s3.listObjects(s3Params, (err, data) => {
					if (err) {
						console.log(err);
						reject(err);
					}
					if (data) {
						resolve(data.Contents);
					}
				});
			});
		});
	}

	public getBucketObject(audioFile: AudioFile): Promise<AudioFile> {
		var s3Params = {
			Bucket: this.bucketName,
			Key: audioFile.getFileName()
		};
		config.update({
			region: 'us-east-1',
		});
		return new Promise((resolve, reject) => {
			this.cognitoService.authGuestUser().then(() => {
				const s3 = new S3();
				s3.getObject(s3Params, ((err, data) => {
					if (err) {
						reject(err);
					}
					else {
						audioFile.setAudioBody(<Uint8Array>data.Body);
						resolve(audioFile);
					}
				}));
			});
		});
	}

	public uploadAudioToBucket(
		file: File,
		genre: Genre,
		username: string,
		password: string
	): Promise<any> {
		return new Promise((resolve) => {
			config.update({
				region: 'us-east-1',
			});
			var s3Params = {
				Bucket: this.bucketName,
				Key: genre.getIdentifier().asString() + '/' + file.name,
				"ContentType": "application/octet-stream",
				Body: file
			};
			this.cognitoService.authCognitoUser(username, password).then(() => {
				const s3 = new S3();
				this.uploadToS3(s3, s3Params).then(() =>
					resolve(true)
				);
			});
		})
	}

	private uploadToS3(s3: S3, s3Params: any): Promise<any> {
		return new Promise((resolve, reject) => {
			s3.upload(s3Params, async (err: Error, data: any) => {
				if (data) {
					resolve(data);
				}
				if (err) {
					reject();
					console.log(err);
				}
			})
		});
	}

	public deleteTrackObject(audioFile: AudioFile): Promise<any> {
		return new Promise((resolve, reject) => {
			const s3 = new S3();
			var s3Params = {
				Bucket: this.bucketName,
				Key: audioFile.getFileName(),
			};
			s3.deleteObject(s3Params, ((err) => {
				if (err) {
					reject(err);
				}
				else {
					resolve(audioFile);
				}
			}));
		});
	}

	public getSignedUrl(s3Params: any): Promise<any> {
		return new Promise((resolve, reject) => {
			const s3 = new S3();
			s3.getSignedUrl('putObject', s3Params, (err: Error, url: string) => {
				if (err) {
					alert('Auth Failed. S3 Upload cannot continue');
					reject(err);
					return '';
				}
				else {
					resolve({ url: url, s3: s3 });
					return url;
				}
			});
		});
	};

	public forceRefresh(): void {
		this.updatedSubject.next(true);
	}

}