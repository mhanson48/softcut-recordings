import { TestBed } from '@angular/core/testing';
import { CognitoService } from './cognito.service';
import { S3Service } from './s3.service';
import { Genre } from '../models/genere.model';
import { AudioFile } from '../models/audio-file.model';
import { GenreIdentifier } from '../models/genre-identifier';

jest.mock("aws-sdk", () => {

	const config = {
		credentials: jest.fn(() => {
			return 'not undefined :)'
		}),
		update: jest.fn(),
		getCredentials: jest.fn((callback) => callback('someCreds')),
	};

	return {
		config: config,
		S3: jest.fn(() => {
			return {
				getSignedUrl: jest.fn((operation, param, callbacks) => {
					console.log(operation, param);
					callbacks(undefined, 'PRESIGNED!')
					callbacks(new Error('errMsg'), undefined);
				}),
				upload: jest.fn((param, callbacks) => {
					console.log(param);
					callbacks(undefined, 'S3 Response');
					callbacks(new Error('S3UploadError'), undefined)
				}),
				listObjects: jest.fn((param, callbacks) => {
					console.log(param);
					callbacks(undefined, 'S3 Response');
					callbacks(new Error('S3ListObjectError'), undefined)
				}),
				getObject: jest.fn((param, callbacks) => {
					console.log(param);
					callbacks(undefined, 'S3 Response');
					callbacks(new Error('S3GetObjectError'), undefined)
				}),
				deleteObject: jest.fn((param, callbacks) => {
					console.log(param);
					callbacks(undefined, 'S3 Response');
					callbacks(new Error('S3GetObjectError'), undefined)
				})
			}
		}),
		CognitoIdentityCredentials: jest.fn(() => {
			return {
				clearCachedId: jest.fn(),
				getPromise: jest.fn().mockResolvedValue(Promise.resolve(true))
			}
		})
	}
});

jest.mock("amazon-cognito-identity-js", () => {

	const mockCognitoSession = {
		getIdToken: jest.fn(() => {
			return { getJwtToken: jest.fn() }
		})
	};

	let mockCognitoUser = {
		authenticateUser: jest.fn((authDetails, callbacks) => {
			console.log(authDetails);
			if (callbacks && callbacks.onSuccess) {
				callbacks.onSuccess(mockCognitoSession);
			};
			if (callbacks && callbacks.onFailure) {
				callbacks.onFailure('');
			}
		}),
		setAuthenticationFlowType: jest.fn(),
	};

	return {
		CognitoUser: jest.fn(() => mockCognitoUser),
		CognitoUserSession: jest.fn(() => mockCognitoSession),
		CognitoUserPool: jest.fn(),
		AuthenticationDetails: jest.fn()
	};
});

describe('S3Service', () => {

	let service: S3Service;

	beforeEach(async () => {
		TestBed.configureTestingModule({
			providers: [
				S3Service,
				{
					provide: CognitoService, useValue: {
						authGuestUser: jest.fn().mockResolvedValue(true),
						authCognitoUser: jest.fn().mockResolvedValue(true),
					}
				}
			]
		});

		service = TestBed.inject(S3Service);
	});

	it('can retrieve objects from an S3 bucket', async () => {
		console.log = jest.fn();
		service.listBucketAudio();
	});

	it('can get bucket audio object data', () => {
		service.getBucketObject(
			new AudioFile(
				'1',
				new GenreIdentifier('1'),
				0
			)
		);
	})

	it('can force refresh s3 objects', () => {
		service.forceRefresh();
	});

	it('can upload audio to S3 bucket', async () => {
		console.log = jest.fn();
		window.alert = jest.fn();
		const blob = new Blob(['someBlobData']);
		const file = new File([blob], 'values.json', {
			type: 'application/JSON',
		})
		service.uploadAudioToBucket(
			file,
			new Genre(
				new GenreIdentifier('testGenreName'),
				'testDesc',
				0
			),
			'mockUsername',
			'mockPassword'
		)
	});

	it('can remove audio from an S3 bucket', async () => {
		const audioFile = new AudioFile('test.mp3', new GenreIdentifier('1'), 0)
		service.deleteTrackObject(audioFile);
	});

	it('can fetch a presigned URL', async () => {
		const urlResponse = service.getSignedUrl({});
		expect(urlResponse).not.toBeUndefined();
	});

});