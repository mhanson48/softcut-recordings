import { TestBed } from '@angular/core/testing';
import { CognitoService } from './cognito.service';

jest.mock("aws-sdk", () => {

	const config = {
		credentials: jest.fn(() => {
			return 'not undefined :)'
		}),
		update: jest.fn(),
		getCredentials: jest.fn((callback) => callback('someCreds'))
	};


	return {
		config: config,
		CognitoIdentityCredentials: jest.fn(() => {
			return {
				clearCachedId: jest.fn(),
				getPromise: jest.fn().mockResolvedValue(Promise.resolve(true))
			}
		})
	}
})

jest.mock("amazon-cognito-identity-js", () => {
	const mockCognitoSession = {
		getIdToken: jest.fn(() => {
			return { getJwtToken: jest.fn() }
		})
	};

	let mockCognitoUser = {
		authenticateUser: jest.fn((authDetails, callbacks) => {
			console.log(authDetails);
			if (callbacks && callbacks.onSuccess) {
				callbacks.onSuccess(mockCognitoSession);
			};
			if (callbacks && callbacks.onFailure) {
				callbacks.onFailure(() => {
					return Promise.reject()
				});
			}
		}),
		setAuthenticationFlowType: jest.fn(),
	};

	return {
		CognitoUser: jest.fn(() => mockCognitoUser),
		CognitoUserSession: jest.fn(() => mockCognitoSession),
		CognitoUserPool: jest.fn(),
		AuthenticationDetails: jest.fn()
	};
});

describe('CognitoService', () => {

	let service: CognitoService;

	beforeEach(async () => {
		TestBed.configureTestingModule({
			imports: [
			],
			declarations: [
			],
			providers: [
				CognitoService
			]
		});

		service = TestBed.inject(CognitoService);
	});

	it('can authenticate an existing cognito user', async () => {
		console.log = jest.fn();
		await expect(service.authCognitoUser(
			'mockUsername',
			'mockPassword'
		).catch((err) => {
			console.log(err);
		})
		);
	});

	it('can auth a guest user', async () => {
		console.log = jest.fn();
		await service.authGuestUser()
			.catch((err) => {
				console.log(err);
			});
	});

});