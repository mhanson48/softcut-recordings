import { Injectable } from "@angular/core";
import { AuthenticationDetails, CognitoUser, CognitoUserPool, CognitoUserSession } from "amazon-cognito-identity-js";
import { CognitoIdentityCredentials, config } from 'aws-sdk';

export const GUEST_COGNITO_USERNAME = 'softcut-guest';
export const GUEST_COGNITO_PASSWORD = 'softcut-guest1';

@Injectable()
export class CognitoService {

	constructor(
	) {
	}

	authCognitoUser(
		username: string,
		password: string,
	): Promise<any> {
		return new Promise((resolve, reject) => {
			const userPoolId = 'us-east-1_PEfV5xse1';
			const appClientId = '7s7tv6jp05hg81igutedbacilg';

			const poolData = {
				UserPoolId: userPoolId,
				ClientId: appClientId
			};

			const userPool = new CognitoUserPool(poolData);
			var userData = {
				Username: username,
				Pool: userPool,
			};
			var authenticationData = {
				Username: username,
				Password: password,
			};


			var authenticationDetails = new AuthenticationDetails(authenticationData);
			var cognitoUser = new CognitoUser(userData);

			cognitoUser.setAuthenticationFlowType('USER_PASSWORD_AUTH');

			cognitoUser.authenticateUser(authenticationDetails, {
				onSuccess: (result: CognitoUserSession) => {
					this.handleAuthSuccess(result).then(() => {
						resolve(result);
					});
				},
				onFailure: (err) => {
					console.log(err);
					reject(err);
				},
			});

		});
	}

	public authGuestUser(): Promise<any> {
		return this.authCognitoUser(
			GUEST_COGNITO_USERNAME,
			GUEST_COGNITO_PASSWORD
		);
	}

	private handleAuthSuccess(result: CognitoUserSession): Promise<any> {
		return new Promise((resolve) => {
			const token = result.getIdToken().getJwtToken();

			let awsConfig = new CognitoIdentityCredentials({
				IdentityPoolId: 'us-east-1:2107499f-aa6f-4b01-aa9c-257568f7e447',
				Logins: {
					'cognito-idp.us-east-1.amazonaws.com/us-east-1_PEfV5xse1': token
				}
			});

			config.credentials = awsConfig;
			awsConfig.clearCachedId();

			awsConfig.getPromise().then(() => {
				resolve(true);
			})
		});
	}
}