import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ArchiveService } from './archive.service';
import { Genre } from './../models/genere.model';
import System from '../../../utils/system';
import { S3Service } from './s3.service';
import { MaterialModule } from 'src/app/material.module';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { AudioFile } from '../models/audio-file.model';
import { GenreIdentifier } from '../models/genre-identifier';

describe('ArchiveService ', () => {

	let httpCtrl: HttpTestingController;
	let service: ArchiveService;

	beforeEach(async () => {
		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				BrowserModule,
				BrowserAnimationsModule,
				FormsModule,
				HttpClientTestingModule,
				MaterialModule
			],
			providers: [
				ArchiveService,
				{
					provide: S3Service, useValue: {
						listBucketAudio: jest.fn()
					}
				}

			]
		});

		service = TestBed.inject(ArchiveService);
		httpCtrl = TestBed.inject(HttpTestingController);
		await TestBed.compileComponents();
	});

	afterEach(() => {
		httpCtrl.verify();
	});

	it('can get the collection of genres & audio files', async () => {
		console.log = jest.fn();
		service.getGenreAndAudio();

		const genres = [
			new Genre(new GenreIdentifier('Metal'), 'Heavy', 0),
			new Genre(new GenreIdentifier('Rock'), 'nRoll', 1)
		];
		let genreRequest = httpCtrl.expectOne(System.apiURL + `/genre`);
		genreRequest.flush(genres);

		const audio = [
			new AudioFile('Metal', new GenreIdentifier('testGenreName'), 0),
		];
		let audioRequest = httpCtrl.expectOne(System.apiURL + `/audio`);
		audioRequest.flush(audio);
	});

	it('can create a new genre', async () => {
		const newGenre = new Genre(new GenreIdentifier('newGenre'), 'genDesc', 0);
		service.createGenre(newGenre).subscribe(data => {
			expect(data.status).toBe(200)
		});
		let request = httpCtrl.expectOne(System.apiURL + `/genre`);
		expect(request.request.method).toBe('POST');
	});

	it('can update an existing genre', async () => {
		const existingGenre = new Genre(new GenreIdentifier('existingGenre'), 'genDesc', 0);
		service.updateGenre(existingGenre).subscribe(data => {
			expect(data.status).toBe(200)
		});
		let request = httpCtrl.expectOne(System.apiURL + `/genre`);
		expect(request.request.method).toBe('PUT');
	});

	it('can delete a genre', async () => {
		await service.deleteGenre(
			new Genre(new GenreIdentifier('testGenre'), 'genDesc', 0)
		).subscribe(data => {
			expect(data.status).toBe(200)
		});
		let request = httpCtrl.expectOne(System.apiURL + `/genre`);
		expect(request.request.method).toBe('DELETE');
	});

	it('can create a new audio file', async () => {
		const genreIdentifier = new GenreIdentifier('Rock');
		const audioFile = new AudioFile(
			'testFileName',
			genreIdentifier,
			0
		);
		await service.createAudio(
			audioFile
		).subscribe(() => { });
		const audio = [
			new AudioFile('Metal', genreIdentifier, 0),
		];

		let request = httpCtrl.expectOne(System.apiURL + `/audio`);
		request.flush(audio);

		expect(request.request.method).toBe('POST');
	});

	it('can delete an audio file', async () => {
		await service.deleteAudio(
			new AudioFile('fileName.mp3', new GenreIdentifier('1'), 1)
		).subscribe(data => {
			expect(data.status).toBe(200)
		});
		let request = httpCtrl.expectOne(System.apiURL + `/audio`);
		expect(request.request.method).toBe('DELETE');
	});

	it('can retrieve a signed s3 url', async () => {
		await service.getSignedS3Url(new AudioFile(
			'testAudioFile',
			new GenreIdentifier('1'),
			0
		)).subscribe(data => {
			expect(data.status).toBe(200)
		});
		let request = httpCtrl.expectOne(System.apiURL + `/audio/signed`);
		expect(request.request.method).toBe('POST');
	});

	it('can upload an audio file using a signed url', async () => {
		await service.putToSignedS3Url(
			'signedUrl', new File([], 'name')
		).subscribe(data => {
			expect(data.status).toBe(200);
		});
		let request = httpCtrl.expectOne(`signedUrl`);
		expect(request.request.method).toBe('PUT');
	})

	it('can update the audio order', async () => {
		await service.updateAudioOrder(
			[new AudioFile('testFile.mp3', new GenreIdentifier("1"), 0)]
		).subscribe(data => {
			expect(data.status).toBe(200);
		});
		let request = httpCtrl.expectOne(System.apiURL + `/audio`);
		expect(request.request.method).toBe('PUT');
	});


});