import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { ArchiveAdminComponent } from './archive-admin.component';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ArchiveService } from '../service/archive.service';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { S3Service } from '../service/s3.service';
import { CognitoService } from '../service/cognito.service';
import { AudioAdminComponent } from './audio-admin/audio-admin.component';
import { GenreAdminComponent } from './genre-admin/genre-admin.component';

describe('ArchiveAdminComponent', () => {

	let component: ArchiveAdminComponent;
	let fixture: ComponentFixture<ArchiveAdminComponent>;

	const dialogMock = {
		close: () => { }
	};

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				BrowserModule,
				BrowserAnimationsModule,
				FormsModule,
				MaterialModule,
			],
			declarations: [
				ArchiveAdminComponent,
				AudioAdminComponent,
				GenreAdminComponent
			],
			providers: [
				{ provide: ArchiveService },
				{ provide: MatDialogRef, useValue: dialogMock },
				{ provide: MAT_DIALOG_DATA, useValue: [] },
				{
					provide: S3Service, usevalue: {
						forceRefresh: jest.fn()
					}
				},
				{ provide: CognitoService, useValue: {
					authCognitoUser: jest.fn().mockResolvedValue({})
				} }
			]
		}).compileComponents();

		TestBed.inject(S3Service);
		fixture = TestBed.createComponent(ArchiveAdminComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('Should be able to close the Archive Admin Component Modal', () => {
		let spy = jest.spyOn(component.dialogRef, 'close');
		component.closeClick();
		expect(spy).toHaveBeenCalledTimes(1);
	});

	it('Should call genre endpoint after logging in', async () => {
		component.adminLoginClick();
	})

});
