import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AudioAdminComponent } from './audio-admin.component';
import { S3Service } from '../../service/s3.service';
import { ArchiveService } from '../../service/archive.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { AudioFile } from '../../models/audio-file.model';
import { GenreIdentifier } from '../../models/genre-identifier';
import { Genre } from '../../models/genere.model';

describe('AudioAdminComponent', () => {

	let component: AudioAdminComponent;
	let fixture: ComponentFixture<AudioAdminComponent>;

	const dialogMock = {
		open: () => { }
	};

	let service: ArchiveService;

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				BrowserModule,
				HttpClientTestingModule,
				BrowserAnimationsModule,
				FormsModule,
				MaterialModule,
			],
			declarations: [
				AudioAdminComponent,
			],
			providers: [
				{ provide: MatDialogRef, useValue: dialogMock },
				{ provide: MAT_DIALOG_DATA, useValue: [] },
				{
					provide: S3Service, useValue: {
						deleteTrackObject: jest.fn().mockResolvedValue(Promise.resolve([{ Key: 'lolol' }])),
					}
				},
				{
					provide: ArchiveService, useValue: {
						allTracks: [],
						genres: [],
						createAudio: jest.fn().mockImplementation(() => of([])),
						deleteAudio: jest.fn().mockImplementation(() => of([])),
						updateAudioOrder: jest.fn().mockImplementation(() => of([])),
						dataRefreshObservable: of()
					}
				}
			]
		}).compileComponents();

		TestBed.inject(HttpTestingController);
		service = TestBed.inject(ArchiveService);
		fixture = TestBed.createComponent(AudioAdminComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});


	it('should be able to upload audio', () => {
		component.uploadAudioClick();
	});

	it('will call the s3 delete object method', () => {
		console.log = jest.fn();
		const spy = jest.spyOn(service, 'deleteAudio');
		const trackObject = new AudioFile('test.mp3', new GenreIdentifier('1'), 0)
		component.deleteTrackClick(trackObject);
		expect(spy).toHaveBeenCalledTimes(1);
	})

	it('can change the genre selection', () => {
		const genre = new Genre(new GenreIdentifier('newGenre'), 'testGenre', 0);
		component.genreChange(genre);
	});

	it('will disable up button for index zero', () => {
		component.trackList = [
			new AudioFile('testFile.mp3', new GenreIdentifier("1"), 0)
		];
		expect(component.disableUpOrder(0)).toBeTruthy();
		expect(component.disableUpOrder(1)).toBeFalsy();
	});

});