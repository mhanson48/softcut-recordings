import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MaterialModule } from 'src/app/material.module';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AudioAdminComponent } from './audio-admin.component';
import { S3Service } from '../../service/s3.service';
import { ArchiveService } from '../../service/archive.service';
import { of } from 'rxjs';
import { AudioFile } from '../../models/audio-file.model';
import { GenreIdentifier } from '../../models/genre-identifier';
import { Genre } from '../../models/genere.model';

describe('AudioAdminComponent', () => {

	let component: AudioAdminComponent;
	let fixture: ComponentFixture<AudioAdminComponent>;

	const dialogMock = {
		open: () => { }
	};

	const genre = new Genre(new GenreIdentifier("1"), 'desc', 0);

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				BrowserModule,
				BrowserAnimationsModule,
				FormsModule,
				MaterialModule,
			],
			declarations: [
				AudioAdminComponent,
			],
			providers: [
				{ provide: MatDialogRef, useValue: dialogMock },
				{ provide: MAT_DIALOG_DATA, useValue: [] },
				{
					provide: S3Service, useValue: {
						deleteTrackObject: jest.fn().mockResolvedValue(Promise.resolve([{ Key: 'lolol' }])),
						forceRefresh: jest.fn()
					}
				},
				{
					provide: ArchiveService, useValue: {
						allTracks: [
							new AudioFile('testFile.mp3', new GenreIdentifier("1"), 0),
							new AudioFile('testFile2.mp3', new GenreIdentifier("1"), 1)
						],
						genres: [
							genre
						],
						updateAudioOrder: jest.fn().mockImplementation(() => of([])),
						dataRefreshObservable: of(),
						selectedGenre: genre
					}
				}
			]
		}).compileComponents();

		fixture = TestBed.createComponent(AudioAdminComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('will update the audio order after saving', () => {
		component.updateAudioOrderClick();
	});

	it('can move the audio order up and down', () => {
		component.moveOrderUp(1);
		component.moveOrderDown(0);
	});

	it('will disable down button for last index', () => {
		component.disableDownOrder(0);
	});

});