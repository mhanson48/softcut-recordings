import { Component, Self } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { S3Service } from '../../service/s3.service';
import { UploadAudioComponent } from '../../upload-audio/upload-audio.component';
import { ArchiveService } from '../../service/archive.service';
import { AudioFile } from '../../models/audio-file.model';
import { Genre } from '../../models/genere.model';
import { MatTableDataSource } from '@angular/material/table';

@Component({
    selector: 'audio-admin',
    templateUrl: './audio-admin.component.html',
    styleUrls: ['./audio-admin.component.scss'],
})
export class AudioAdminComponent {

    genres: Genre[];
    selectedGenre: Genre;
    allTracks: AudioFile[] = [];
    trackList: AudioFile[] = [];
    orderChanged: boolean = false;

    audioDataSource: MatTableDataSource<AudioFile> = new MatTableDataSource();
    audioColumns = ['name', 'genre', 'order', 'actions'];

    constructor(
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<Self>,
        public archiveService: ArchiveService,
        public s3Service: S3Service
    ) {
        this.setupTrackList();

        this.archiveService.dataRefreshObservable.subscribe(() => {
            this.genres = this.archiveService.genres;
            this.selectedGenre = this.archiveService.selectedGenre;
            this.allTracks = this.archiveService.allTracks;
            this.filterAudio();
        });
    }

    private setupTrackList(): void {
        this.genres = this.archiveService.genres;
        this.selectedGenre = this.archiveService.selectedGenre;

        this.allTracks = this.archiveService.allTracks;
        this.filterAudio();
    }

    public genreChange(genre: Genre): void {
        this.selectedGenre = genre;
        this.filterAudio();
    }

    private filterAudio(): void {
        this.trackList = this.allTracks.filter(t => t.getGenreIdentifier().asString() == this.selectedGenre.getIdentifier().asString())
        this.audioDataSource.data = this.trackList;
    }

    public uploadAudioClick(): void {
        const modalRef = this.dialog.open(UploadAudioComponent);
        (<UploadAudioComponent>modalRef.componentInstance).selectedGenre = this.selectedGenre;
    }

    public updateAudioOrderClick(): void {
        this.archiveService.updateAudioOrder(this.trackList).subscribe(() => {
        });
    }

    public deleteTrackClick(audioFile: AudioFile): void {
        this.archiveService.deleteAudio(audioFile).subscribe(() => {
            this.setupTrackList();
            this.s3Service.forceRefresh();
        });
    }

    public moveOrderUp(index: number): void {
        this.bumpAudioArrayElement(index, index - 1);
        this.refreshTableData();
        this.orderChanged = true;
    }

    public moveOrderDown(index: number): void {
        this.bumpAudioArrayElement(index, index + 1);
        this.refreshTableData();
        this.orderChanged = true;
    }

    disableUpOrder(index: number) {
        return index == 0;
    }

    disableDownOrder(index: number) {
        return index == this.trackList.length - 1;
    }

    bumpAudioArrayElement(fromIndex: number, toIndex: number): void {
        let removedItem = this.trackList.splice(fromIndex, 1)[0];
        this.trackList.splice(toIndex, 0, removedItem);
        let index = 0;
        this.trackList.forEach((track) => {
            track.updateOrder(index);
            index++;
        });
        this.refreshTableData();
    }

    refreshTableData(): void {
        this.audioDataSource.data = this.audioDataSource.data;
    }

}