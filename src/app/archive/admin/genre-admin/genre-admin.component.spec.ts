import { ComponentFixture, TestBed } from "@angular/core/testing";
import { ArchiveService } from "../../service/archive.service";
import { GenreAdminComponent } from "./genre-admin.component";
import { HttpClientTestingModule, HttpTestingController } from "@angular/common/http/testing";
import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from "@angular/forms";
import { MaterialModule } from "src/app/material.module";
import { S3Service } from "../../service/s3.service";
import { CognitoService } from "../../service/cognito.service";
import { of } from "rxjs";
import { Genre } from "../../models/genere.model";
import System from "src/utils/system";
import { MAT_DIALOG_DATA, MatDialogRef } from "@angular/material/dialog";
import { GenreIdentifier } from "../../models/genre-identifier";

describe('GenreAdminComponent', () => {

	let service: ArchiveService;
	let component: GenreAdminComponent;
	let fixture: ComponentFixture<GenreAdminComponent>;
	let httpCtrl: HttpTestingController;

	const dialogMock = {
		close: () => { }
	};

	beforeEach(() => {
		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				BrowserModule,
				BrowserAnimationsModule,
				HttpClientTestingModule,
				FormsModule,
				MaterialModule,
			],
			declarations: [
				GenreAdminComponent
			],
			providers: [
				{ provide: ArchiveService },
				{ provide: S3Service, useValue: {} },
				{ provide: CognitoService, useValue: {} },
				{
					provide: MatDialogRef, useValue: dialogMock
				},
				{
					provide: MAT_DIALOG_DATA, useValue: []
				}
			]
		}).compileComponents();

		httpCtrl = TestBed.inject(HttpTestingController);
		service = TestBed.inject(ArchiveService);
		fixture = TestBed.createComponent(GenreAdminComponent);
		component = fixture.componentInstance;
		fixture.detectChanges();
	});

	it('Should create a new genre when the Save button is clicked', () => {
		const spy = jest.spyOn(service, 'createGenre').mockImplementation(() => of([]));
		component.saveNewGenreClick();
		expect(spy).toHaveBeenCalledTimes(1);

		let request = httpCtrl.expectOne(System.apiURL + '/genre');
		request.flush({});
		expect(request.request.method).toBe('GET');
	})

	it('Should reload the genre collection after adding a new genre', () => {
		const createSpy = jest.spyOn(service, 'createGenre').mockImplementation(() => of([]));
		const getGenreSpy = jest.spyOn(service, 'getGenreAndAudio').mockImplementation();
		component.addGenreClick();
		component.saveNewGenreClick();
		expect(createSpy).toHaveBeenCalledTimes(1);
		fixture.detectChanges();
		expect(getGenreSpy).toHaveBeenCalledTimes(1);
		expect(component.addGenreMode).toBeFalsy();
	});

	it('Should delete a genre when the Delete button is clicked', async () => {
		const spy = jest.spyOn(service, 'deleteGenre').mockImplementation(() => of([]));
		await component.deleteGenre(
			new Genre(new GenreIdentifier('mockGenre'), 'genDesc', 0)
		);
		fixture.detectChanges();
		expect(spy).toHaveBeenCalledTimes(1);
	});

	it('should reload the genre collection after deleting an existing one', () => {
		const deleteSpy = jest.spyOn(service, 'deleteGenre').mockImplementation(() => of([]));
		const getSpy = jest.spyOn(service, 'getGenreAndAudio');
		expect(getSpy).not.toHaveBeenCalled();
		component.deleteGenre(new Genre(new GenreIdentifier('mockGenre'), 'genDesc', 0));
		expect(deleteSpy).toHaveBeenCalledTimes(1);
		fixture.detectChanges();
		expect(getSpy).toHaveBeenCalledTimes(1);
	});

	it('can update a genre', async () => {
		const updateSpy = jest.spyOn(service, 'updateGenre').mockImplementation(() => of([]));
		component.updateGenre(new Genre(new GenreIdentifier('editGenre'), 'genDesc', 0));
		expect(updateSpy).toHaveBeenCalledTimes(1);
	});

	it('should reload the genre collection after updating a genre', async () => {
		const updateSpy = jest.spyOn(service, 'updateGenre').mockImplementation(() => of([]));
		const getSpy = jest.spyOn(service, 'getGenreAndAudio');
		expect(getSpy).not.toHaveBeenCalled();
		component.updateGenre(new Genre(new GenreIdentifier('mockGenre'), 'genDesc', 0));
		expect(updateSpy).toHaveBeenCalledTimes(1);
		fixture.detectChanges();
		expect(getSpy).toHaveBeenCalledTimes(1);
		expect(component.updatedGenre).toBeUndefined();
	});

	it('will enter add genre mode and disable edit mode', async () => {
		component.editGenreClick(new Genre(new GenreIdentifier('edit'), 'genDesc', 0));
		expect(component.editGenreMode).toBeTruthy();
		expect(component.addGenreMode).toBeFalsy();
		component.addGenreClick();
		expect(component.addGenreMode).toBeTruthy();
		expect(component.editGenreMode).toBeFalsy();
		expect(component.updatedGenre).toBeUndefined();
	});

	it('will enter edit genre mode and disable add mode', async () => {
		component.addGenreClick();
		expect(component.addGenreMode).toBeTruthy();
		expect(component.editGenreMode).toBeFalsy();
		component.editGenreClick(new Genre(new GenreIdentifier('edit'), 'genDesc', 0));
		expect(component.editGenreMode).toBeTruthy();
		expect(component.addGenreMode).toBeFalsy();
	});

	it('will disable Add button when entering edit mode', () => {
		component.editGenreClick(new Genre(new GenreIdentifier('edit'), 'genDesc', 0));
		fixture.detectChanges();
		const addGenreButton = document.getElementById('addGenreButton');
		expect(addGenreButton).not.toBeUndefined();
		expect(addGenreButton).toHaveProperty('disabled', true);
	});

	it('can open modal to upload new audio file', () => {
		let spy = jest.spyOn(component.dialog, 'open');
		component.uploadAudioClick();
		expect(spy).toHaveBeenCalledTimes(1);
	});

	it('will enable Add button after saving an edited genre', () => {
		const genre = new Genre(new GenreIdentifier('edit'), 'genDesc', 0);
		component.editGenreClick(genre);
		component.updateGenre(genre);
		fixture.detectChanges();
		const addGenreButton = document.getElementById('addGenreButton');
		expect(addGenreButton).not.toBeUndefined();
		expect(addGenreButton).toHaveProperty('disabled', false);
	});

	it('will close the dialog on close click', () => {
		let spy = jest.spyOn(component.dialogRef, 'close');
		component.closeClick();
		expect(spy).toHaveBeenCalledTimes(1);
	});
	
	it('will set the max genre order genres already exist', () => {
		component.genres = [new Genre(
			new GenreIdentifier("genre1"),
			'testDesc',
			0
		)];
		component.setGenreOrder();
		expect(component.genreOrder).toBeGreaterThan(0);
	});

	it('will set the initial edit genre order to 0 when no genres exist', () => {
		expect(component.genreOrder).toBe(0);
	});

	it('will show the edit field when the genre is selected', () => {
		const genre = new Genre(new GenreIdentifier('1'), '1', 1);
		component.updatedGenre = genre;
		component.showEditField(genre)
	})

});