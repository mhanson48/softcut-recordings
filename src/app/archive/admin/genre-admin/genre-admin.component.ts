import { Component, Self } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ArchiveService } from '../../service/archive.service';
import { Genre } from '../../models/genere.model';
import { UploadAudioComponent } from '../../upload-audio/upload-audio.component';
import { GenreIdentifier } from '../../models/genre-identifier';

@Component({
    selector: 'genre-admin',
    templateUrl: './genre-admin.component.html',
    styleUrls: ['./genre-admin.component.scss'],
})
export class GenreAdminComponent {

    genres: Genre[] = [];
    genreColumns = ['name', 'description', 'order', 'action'];

    genreName: string = '';
    genreDescription: string = '';
    genreOrder: number = 0;

    addGenreMode: boolean = false;
    editGenreMode: boolean = false;
    updatedGenre: Genre | undefined;

    constructor(
        private archiveService: ArchiveService,
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<Self>
    ) {
        this.genres = this.archiveService.genres;

        this.archiveService.dataRefreshObservable.subscribe(() => {
            this.genres = this.archiveService.genres;
        })
    }

    public addGenreClick(): void {
        this.addGenreMode = true;
        this.editGenreMode = false;
        this.updatedGenre = undefined;
        this.setGenreOrder();
    }

    public editGenreClick(genre: Genre): void {
        this.editGenreMode = true;
        this.addGenreMode = false;
        this.updatedGenre = genre;
    }

    showEditField(genre: Genre): boolean {
        const genreIdentifier = genre.getIdentifier().asString();
        let updatedGenreIdentifier = '';
        if (this.updatedGenre != undefined) {
            updatedGenreIdentifier = this.updatedGenre.getIdentifier().asString();
        }
        return this.editGenreMode || updatedGenreIdentifier == genreIdentifier;
    }

    public updateGenre(genre: Genre): void {
        this.archiveService.updateGenre(genre).subscribe(() => {
            this.archiveService.getGenreAndAudio().then(() => {
                this.archiveService.forceRefresh();
            });
        });
        this.updatedGenre = undefined;
        this.editGenreMode = false;
    }

    public deleteGenre(genre: Genre): void {
        this.archiveService.deleteGenre(genre).subscribe(() => {
            this.archiveService.getGenreAndAudio().then(() => {
                this.archiveService.forceRefresh();
            });
        });
    }

    public saveNewGenreClick(): void {
        const genre = new Genre(
            new GenreIdentifier(this.genreName),
            this.genreDescription,
            this.genreOrder
        );
        this.archiveService.createGenre(genre).subscribe(() => {
            this.archiveService.getGenreAndAudio().then(() => {
                this.archiveService.forceRefresh();
            });
        });
        this.addGenreMode = false;
    }

    public uploadAudioClick(): void {
        this.dialog.open(UploadAudioComponent);
    }

    public setGenreOrder(): void {
        if (this.genres.length == 0) {
            this.genreOrder = 0;
        }
        else {
            const maxOrder = this.genres.reduce((a, b) => a.getOrder() > b.getOrder() ? a : b).getOrder() + 1;
            this.genreOrder = maxOrder;
        }
    }

    public closeClick(): void {
        this.dialogRef.close();
    }

}