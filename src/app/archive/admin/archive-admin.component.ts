import { Component, Self } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { CognitoService } from '../service/cognito.service';
import { config } from "aws-sdk";

@Component({
    selector: 'archive-admin',
    templateUrl: './archive-admin.component.html',
    styleUrls: ['./archive-admin.component.scss'],
})
export class ArchiveAdminComponent {

    adminAuthed: boolean = false;
    adminUsername: string = '';
    adminPassword: string = '';

    constructor(
        public dialog: MatDialog,
        public dialogRef: MatDialogRef<Self>,
        public cognitoService: CognitoService
    ) { }

    public adminLoginClick(): void {
        config.update({
            region: 'us-east-1',
        });
        this.cognitoService.authCognitoUser(
            this.adminUsername,
            this.adminPassword
        ).then(() => {
            this.adminAuthed = true;
        })
    }

    public closeClick(): void {
        this.dialogRef.close();
    }

}