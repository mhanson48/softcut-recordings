import { GenreIdentifier } from "./genre-identifier";

export class AudioFile {

	private audioBody: Uint8Array | string | ArrayBuffer | null = null;

	constructor(
		private readonly fileName: string,
		private readonly genreIdentifier: GenreIdentifier,
		private order: number
	) {
	}

	getFileName(): string {
		return this.fileName;
	}

	getGenreIdentifier(): GenreIdentifier {
		return this.genreIdentifier;
	}

	setAudioBody(body: Uint8Array | string | ArrayBuffer | null): void {
		this.audioBody = body;
	}

	getAudioBody(): Uint8Array | string | ArrayBuffer | null {
		return this.audioBody;
	}

	getOrder(): number {
		return this.order;
	}

	updateOrder(order: number): void {
		this.order = order;
	}

	public static fromJson(json: any): AudioFile[] {
		let audioFiles = [];
		for (let track of json) {
			audioFiles.push(
				new AudioFile(
					track.trackName,
					new GenreIdentifier(track.genre),
					parseInt(track.order)
				)
			);
		}
		return audioFiles;
	}
}