export class GenreIdentifier {
	constructor(private readonly genre: string) {
	}

	public asString(): string {
		return this.genre;
	}
}