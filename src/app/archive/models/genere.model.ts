import { GenreIdentifier } from "./genre-identifier";

export class Genre {
	constructor(
		private readonly identifier: GenreIdentifier,
		private readonly description: string,
		private readonly order: number
	) {
	}

	getIdentifier(): GenreIdentifier {
		return this.identifier;
	}

	getDescription(): string {
		return this.description;
	}

	getOrder(): number {
		return this.order;
	}

	public static fromJson(json: any): Genre[] {
		let genres = [];
		for (let genre of json) {
			genres.push(
				new Genre(
					new GenreIdentifier(genre.genre),
					genre.description,
					parseInt(genre.order)
				)
			);
		}
		return genres;
	}

}