import { AudioFile } from './audio-file.model';
import { GenreIdentifier } from './genre-identifier';

describe('Audio File Model Test', () => {

	it('should be able to create a file path', (() => {
		const genreIdentifier = new GenreIdentifier('Metal');

		const fileName = 'Herselph.mp3';
		let audioFile = new AudioFile(fileName, genreIdentifier, 0);

		expect(audioFile.getFileName()).toBe(fileName);
		expect(audioFile.getGenreIdentifier()).toBe(genreIdentifier);
		expect(audioFile.getAudioBody()).toBeNull();
	}));

	it('should be able to update audio body information', (() => {
		let audioFile = new AudioFile(
			'1',
			new GenreIdentifier('1'),
			0
		);
		const body = new Uint8Array();
		audioFile.setAudioBody(body);
		expect(audioFile.getAudioBody()).toBe(body);
	}));

	it('can get the audio file order', (() => {
		let audioFile = new AudioFile(
			'1',
			new GenreIdentifier('1'),
			0
		);
		audioFile.getOrder();
	}));

	it('can update the audio file order', (() => {
		let audioFile = new AudioFile(
			'1',
			new GenreIdentifier('1'),
			0
		);
		audioFile.updateOrder = jest.fn();
		expect(audioFile.getOrder()).toBe(0);
	}));

	it('can create a list of audio files from json', () => {
		const json = JSON.parse(`[
			{
				"genre": "Metal",
				"trackName": "Ninefold Museum.mp3",
				"order": "10"
			},
			{
				"genre": "Rock",
				"trackName": "Vapor.mp3",
				"order": "7"
			}
		]`
		);
		const audioFiles = AudioFile.fromJson(json)
		expect(audioFiles).toHaveLength(2);
	});
});