import { Genre } from './genere.model';

describe('Genre Model Test', () => {

	it('can create a list of audio files from json', () => {
		const json = JSON.parse(`
			[
				{
					"description": "Synths and stuff",
					"genre": "Electronic",
					"order": "5"
				},
				{
					"description": "Lots of Distortion",
					"genre": "Metal",
					"order": "1"
				}
			]
		`);
		const genres = Genre.fromJson(json)
		expect(genres).toHaveLength(2);
	});
});