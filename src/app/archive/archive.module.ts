import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { ControlsModule } from '../controls/controls.module';
import { ArchiveService } from './service/archive.service';
import { MaterialModule } from '../material.module';
import { AudioPlayerComponent } from '../controls/audio-player/audio-player.component';
import { ArchiveAdminComponent } from './admin/archive-admin.component';
import { FormsModule } from '@angular/forms';
import { UploadAudioComponent } from './upload-audio/upload-audio.component';
import { S3Service } from './service/s3.service';
import { CognitoService } from './service/cognito.service';
import { GenreAdminComponent } from './admin/genre-admin/genre-admin.component';
import { AudioAdminComponent } from './admin/audio-admin/audio-admin.component';
import { ExperimentalToolsComponent } from '../controls/experimental-tools/experimental-tools.component';

const archiveRoutes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
	{ path: 'audio-player', component: AudioPlayerComponent},
	{ path: 'experimental-tools', component: ExperimentalToolsComponent}
]

@NgModule({
	declarations: [
		AudioPlayerComponent,
		ArchiveAdminComponent,
		GenreAdminComponent,
		AudioAdminComponent,
		UploadAudioComponent,
		ExperimentalToolsComponent
	],
	imports: [
		CommonModule,
		BrowserModule,
		RouterModule.forChild(archiveRoutes),
		FormsModule,
		MaterialModule,
		ControlsModule
	],
	providers: [
		ArchiveService,
		S3Service,
		CognitoService
	],
	schemas: [
		CUSTOM_ELEMENTS_SCHEMA
	]
})
export class ArchiveModule { }
