import { Component, OnInit } from '@angular/core';
import { MediaLink } from 'src/app/controls/media-group/media-link.model';

@Component({
    selector: 'bass',
    templateUrl: './bass.component.html',
    styleUrls: ['./bass.component.scss']
})
export class BassComponent implements OnInit {

    mediaLinks: MediaLink[] = [];

    constructor() { }

    ngOnInit(): void {
        this.setupMediaLinks();
    }

    setupMediaLinks() {
        const crunchySwellsLink = new MediaLink('Crunchy Swells', 'OFEnpBl2KPs');
        const SDGR5DemoLink = new MediaLink('SDGR5 Demo', 'Fuwp8kUvUik');
        this.mediaLinks.push(crunchySwellsLink, SDGR5DemoLink);
    }

}
