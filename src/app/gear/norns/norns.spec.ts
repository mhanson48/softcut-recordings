
import { TestBed, async } from '@angular/core/testing';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { NornsComponent } from 'src/app/gear/norns/norns.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';


describe('Norns Component', () => {

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				MatTabsModule,
				MatListModule,
				HttpClientTestingModule
			],
			declarations: [
				NornsComponent,
			],
			schemas: [NO_ERRORS_SCHEMA]
		}).compileComponents();

	}));

	it('should create the norns component', async(() => {
		const fixture = TestBed.createComponent(NornsComponent);
		const nornsComponent = fixture.debugElement.componentInstance;
		expect(nornsComponent).toBeTruthy();

		nornsComponent.ngOnInit();
		expect(nornsComponent.mediaLinks).toHaveLength(6);

	}));
});