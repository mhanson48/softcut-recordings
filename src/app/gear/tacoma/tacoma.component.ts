import { Component, OnInit } from '@angular/core';
import { MediaLink } from 'src/app/controls/media-group/media-link.model';

@Component({
    selector: 'tacoma',
    templateUrl: './tacoma.component.html',
    styleUrls: ['./tacoma.component.scss']
})
export class TacomaComponent implements OnInit {

    mediaLinks: MediaLink[] = [];

    constructor() { }

    ngOnInit(): void {
        this.setupMediaLinks();
    }

    setupMediaLinks() {
        const tacomaLink = new MediaLink('Tacoma', 'tacoma');
        this.mediaLinks.push(tacomaLink);
    }

}
