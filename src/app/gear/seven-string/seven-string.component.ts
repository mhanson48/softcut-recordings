import { Component } from '@angular/core';
import { MediaLink } from 'src/app/controls/media-group/media-link.model';

@Component({
  selector: 'app-seven-string',
  templateUrl: './seven-string.component.html',
  styleUrls: ['./seven-string.component.scss']
})
export class SevenStringComponent {

    mediaLinks: MediaLink[] = [];

    constructor() {
      this.setupMediaLinks();
    }

    setupMediaLinks() {

        // In The Mix
        const inTheMixLink = new MediaLink('In The Mix', '9sEgtDsmjJo');
        this.mediaLinks.push(inTheMixLink);

    }

}
