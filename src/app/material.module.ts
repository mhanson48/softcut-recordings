import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSliderModule } from '@angular/material/slider'
import { MatToolbarModule } from '@angular/material/toolbar'
import { MatCardModule } from '@angular/material/card'
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSelectModule } from '@angular/material/select';
import { MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';


@NgModule({
	imports: [
		MatButtonModule,
		MatMenuModule,
		MatListModule,
		MatIconModule,
		MatTabsModule,
		MatSliderModule,
		MatToolbarModule,
		MatCardModule,
		MatSidenavModule,
		MatInputModule,
		MatDialogModule,
		MatTableModule,
		MatSelectModule,
		MatProgressSpinnerModule,
		FormsModule,
		MatFormFieldModule,
		MatInputModule
	],
	exports: [
		MatButtonModule,
		MatMenuModule,
		MatListModule,
		MatIconModule,
		MatTabsModule,
		MatSliderModule,
		MatToolbarModule,
		MatCardModule,
		MatSidenavModule,
		MatInputModule,
		MatDialogModule,
		MatTableModule,
		MatSelectModule,
		MatProgressSpinnerModule,
		FormsModule,
		MatFormFieldModule,
		MatInputModule
	],
	providers: [
		{ provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'outline' } }
	]
})
export class MaterialModule { }
