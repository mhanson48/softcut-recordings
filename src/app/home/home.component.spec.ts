
import { TestBed, async } from '@angular/core/testing';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HomeComponent } from './home.component';

describe('Home Component', () => {

	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				MatTabsModule,
				MatListModule,
				HttpClientTestingModule,
			],
			declarations: [
				HomeComponent,
			],
			providers: [
			]
		})
		.compileComponents();

	}));

	it('can be created', async(() => {
		const fixture = TestBed.createComponent(HomeComponent);
		const home = fixture.debugElement.componentInstance;

		expect(home).toBeTruthy();
		home.ngOnInit();
	}))
});