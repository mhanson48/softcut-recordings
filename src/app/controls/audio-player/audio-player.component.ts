
import { ElementRef, HostListener, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ArchiveService } from 'src/app/archive/service/archive.service';
import { AudioFile } from 'src/app/archive/models/audio-file.model';
import { Genre } from 'src/app/archive/models/genere.model';
import { ArchiveAdminComponent } from 'src/app/archive/admin/archive-admin.component';
import { S3Service } from 'src/app/archive/service/s3.service';

@Component({
    selector: 'audio-player',
    templateUrl: './audio-player.component.html',
    styleUrls: ['./audio-player.component.scss'],
})

export class AudioPlayerComponent {

    @ViewChild('scrubRangeRef', { static: true }) scrubRangeRef: ElementRef<HTMLInputElement>;
    @ViewChild('volumeRangeRef', { static: true }) volumeRangeRef: ElementRef<HTMLInputElement>;
    @ViewChild('rateRangeRef', { static: false }) rateRangeRef: ElementRef<HTMLInputElement>;

    audioElement: HTMLAudioElement;

    allTracks: AudioFile[] = [];
    trackList: AudioFile[] = [];
    trackIndex: number = 0;
    trackName: string = '';
    updateTimer: NodeJS.Timeout | number | undefined;
    duration: string = "00:00";
    isPlaying: boolean = false;
    showCircuitBending: boolean = false;

    seekSlider: number = 0;
    seekValue: number = 0;
    currentTime: number = 0;
    formattedTotalTime: string = "00:00";
    formattedPlayTime: string = "00:00";
    formattedDuration: string = "00:00";

    genres: Genre[] = [];
    selectedGenre: Genre;

    constructor(
        private archiveService: ArchiveService,
        private s3Service: S3Service,
        private matDialog: MatDialog,
        private router: Router
    ) {
        this.audioElement = new Audio();

        this.router.events.subscribe(() => {
            this.pauseTrack();
        });

        this.archiveService.getGenreAndAudio().then((genreAudioData) => {
            this.genres = genreAudioData[0];
            this.allTracks = genreAudioData[1];
            this.selectedGenre = this.genres[0];
            this.setupTrackList();
        });

        this.archiveService.dataRefreshObservable.subscribe(() => {
            this.genres = this.archiveService.genres;
            this.selectedGenre = this.archiveService.selectedGenre;
            this.allTracks = this.archiveService.allTracks;
            this.setupTrackList();
        })
    }

    setupTrackList(): void {
        this.trackList = this.allTracks.filter(t => t.getGenreIdentifier().asString() == this.selectedGenre.getIdentifier().asString());
    }

    genreChange(): void {
        this.archiveService.selectedGenre = this.selectedGenre;
        this.setupTrackList();
    }

    playButtonClick(): void {
        if (!this.isPlaying) {
            this.playTrack();
        }
        else {
            this.pauseTrack();
        }
    }

    playTrack(): void {
        this.audioElement.play();
        this.isPlaying = true;
    }

    pauseTrack(): void {
        this.audioElement.pause();
        this.isPlaying = false;
    }

    changeTrack(direction: string): void {
        if (direction == 'next') {
            this.trackIndex = (this.trackIndex < this.trackList.length - 1) ? this.trackIndex + 1 : this.trackIndex;
        }
        else {
            this.trackIndex = (this.trackIndex > 0) ? this.trackIndex - 1 : 0;
        }
        const track = this.trackList[this.trackIndex];
        this.loadTrack(track);
        if (this.isPlaying) {
            this.audioElement.play();
        }
    }

    trackClick(track: AudioFile): void {
        this.trackIndex = this.trackList.indexOf(track);
        this.s3Service.getBucketObject(track).then(() => {
            this.loadTrack(track);
            this.playTrack();
        });
    }

    changeVolume(): void {
        this.audioElement.volume = parseInt(this.volumeRangeRef.nativeElement.value) / 100;
    }

    volumeButtonClick(direction: string): void {
        if (direction == 'down') {
            this.audioElement.volume = 0;
            this.volumeRangeRef.nativeElement.value = '0';
        }
        else if (direction == 'up') {
            this.audioElement.volume = 1;
            this.volumeRangeRef.nativeElement.value = '100';
        }
    }

    scrubAudio() {
        this.seekValue = this.audioElement.duration * (parseInt(this.scrubRangeRef.nativeElement.value) / 100);
        this.currentTime = this.seekValue;
        this.audioElement.currentTime = this.currentTime;
    }

    changeRate(): void {
        this.audioElement.playbackRate = parseInt(this.rateRangeRef.nativeElement.value) / 100;
    }

    rateResetClick() {
        this.audioElement.playbackRate = 1;
        this.rateRangeRef.nativeElement.value = "99";
    }

    loadTrack(track: AudioFile) {
        clearInterval(this.updateTimer);
        this.resetValues();

        const audioBody = track.getAudioBody();
        if (audioBody) {
            //@ts-ignore
            const blob = new Blob([audioBody], { type: 'audio/mp3' });
            this.audioElement.src = window.URL.createObjectURL(blob);
            this.audioElement.load();
            this.trackName = this.trackList[this.trackIndex].getFileName();
            this.updateTimer = setInterval(() => { this.seekUpdate() }, 100);
        }
    }

    resetValues() {
        this.currentTime = 0;
        this.duration = "00:00";
        this.seekSlider = 0;
    }

    seekUpdate() {
        let seekPosition = 0;

        if (!isNaN(this.audioElement.duration)) {
            seekPosition = this.audioElement.currentTime * (100 / this.audioElement.duration);
            this.scrubRangeRef.nativeElement.value = seekPosition.toString();

            let totalMinutes = Math.floor(this.audioElement.duration / 60);
            let totalSeconds = Math.floor(this.audioElement.duration - totalMinutes * 60);
            let currentMinutes = Math.floor(this.audioElement.currentTime / 60);
            let currentSeconds = Math.floor(this.audioElement.currentTime - currentMinutes * 60);

            this.formattedTotalTime = (totalMinutes < 10) ? '0' + totalMinutes.toString() + ':' : totalMinutes.toString() + ':';
            this.formattedTotalTime += (totalSeconds < 10) ? '0' + totalSeconds.toString() : totalSeconds.toString();

            this.formattedPlayTime = (currentMinutes < 10) ? '0' + currentMinutes.toString() + ':' : currentMinutes.toString() + ':';
            this.formattedPlayTime += (currentSeconds < 10) ? '0' + currentSeconds.toString() : currentSeconds.toString();

            let trackDuration = this.audioElement.duration - (currentSeconds + (currentMinutes * 60));
            let durationMinutes = Math.floor(trackDuration / 60);
            let durationSeconds = Math.floor(trackDuration - (durationMinutes * 60));

            this.formattedDuration = (durationMinutes < 10) ? '0' + durationMinutes.toString() + ':' : durationMinutes.toString() + ':';
            this.formattedDuration += (durationSeconds < 10) ? '0' + durationSeconds.toString() : durationSeconds.toString();

        }
    }

    circuitBendClick() {
        this.showCircuitBending = !this.showCircuitBending;
    }

    @HostListener('document:keydown', ['$event']) onKeyDown(e: KeyboardEvent) {
        if (e.ctrlKey && e.altKey && e.key == 'm') {
            this.matDialog.open(
                ArchiveAdminComponent,
                {
                    disableClose: true,
                    data: {
                        selectedGenre: this.selectedGenre
                    }
                }
            );
        }
    }

}