import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { AudioFile } from '../../archive/models/audio-file.model';
import { AudioPlayerComponent } from './audio-player.component';
import { Genre } from 'src/app/archive/models/genere.model';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { NornsComponent } from 'src/app/gear/norns/norns.component';
import { ArchiveService } from 'src/app/archive/service/archive.service';
import { MatDialogRef } from '@angular/material/dialog';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { S3Service } from 'src/app/archive/service/s3.service';
import { MaterialModule } from 'src/app/material.module';
import { ArchiveAdminComponent } from 'src/app/archive/admin/archive-admin.component';
import { AudioAdminComponent } from 'src/app/archive/admin/audio-admin/audio-admin.component';
import { GenreAdminComponent } from 'src/app/archive/admin/genre-admin/genre-admin.component';
import { GenreIdentifier } from 'src/app/archive/models/genre-identifier';
import { CognitoService } from 'src/app/archive/service/cognito.service';
import { of } from 'rxjs';

describe('Audio Player Component', () => {

	let router: Router;
	let fixture: ComponentFixture<AudioPlayerComponent>;
	let mediaPlayer: AudioPlayerComponent;

	let archiveService: ArchiveService;
	let s3Service: S3Service;
	const genreIdentifier = new GenreIdentifier('testGenreName');
	console.log = jest.fn();

	beforeEach(async(() => {

		TestBed.configureTestingModule({
			imports: [
				CommonModule,
				BrowserModule,
				BrowserAnimationsModule,
				HttpClientTestingModule,
				FormsModule,
				NoopAnimationsModule,
				MaterialModule,
				RouterTestingModule.withRoutes(
					[{ path: 'norns', component: NornsComponent }]
				)
			],
			declarations: [
				AudioPlayerComponent,
				ArchiveAdminComponent,
				AudioAdminComponent,
				GenreAdminComponent
			],
			providers: [
				{
					provide: ArchiveService, useValue: {
						getGenres: jest.fn().mockResolvedValue(Promise.resolve([
							new Genre(
								new GenreIdentifier('testGenre'),
								'2',
								0
							)])),
						getAudio: jest.fn().mockResolvedValue(Promise.resolve([])),
						getGenreAndAudio: jest.fn().mockResolvedValue(Promise.resolve([
							[],
							[]
						])),
						dataRefreshObservable: of()
					}
				},
				{ provide: MatDialogRef, useValue: {} },
				{
					provide: S3Service, useValue: {
						// Dude... Comment out the mockResolvedValue and check what happens... Days... Days of debugging this shit.
						/**
						 * TypeError: Converting circular structure to JSON
								--> starting at object with constructor 'Zone'
								|     property '_zoneDelegate' -> object with constructor '_ZoneDelegate'
								--- property 'zone' closes the circle
								at stringify (<anonymous>)
			
							at messageParent (node_modules/jest-worker/build/workers/messageParent.js:29:19)
						 */
						getBucketObject: jest.fn().mockResolvedValue(Promise.resolve(
							new AudioFile(
								'1',
								genreIdentifier,
								0
							)))
					}
				},
				{
					provide: CognitoService, useValue: {}
				}
			]
		}).compileComponents();

		archiveService = TestBed.inject(ArchiveService);
		s3Service = TestBed.inject(S3Service);

		router = TestBed.get(Router);
		fixture = TestBed.createComponent(AudioPlayerComponent);
		mediaPlayer = fixture.componentInstance;
		fixture.detectChanges();

	}));

	it('should load the audio player', () => {
		console.log = jest.fn();
		const serviceSpy = jest.spyOn(archiveService, 'getGenreAndAudio');
		expect(serviceSpy).toHaveBeenCalled();
		mediaPlayer.audioElement.load = jest.fn();
		global.setInterval = jest.fn();
		expect(mediaPlayer).toBeTruthy();
	});

	it('can change the genre', () => {
		console.log = jest.fn();
		const genre = new Genre(
			new GenreIdentifier('testGenre'),
			'1',
			0
		);
		mediaPlayer.genres = [genre];
		mediaPlayer.genreChange();
	});

	it(`play button should start & pause playpack`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();
		mediaPlayer.audioElement.pause = jest.fn();

		let testTrack = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		mediaPlayer.trackList = [testTrack];
		expect(mediaPlayer.isPlaying).toBeFalsy();

		mediaPlayer.playButtonClick();
		expect(mediaPlayer.isPlaying).toBeTruthy();

		mediaPlayer.playButtonClick();
		expect(mediaPlayer.isPlaying).toBeFalsy();

		jest.clearAllMocks();
	});

	it(`should stop playback on router navigation`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();
		mediaPlayer.audioElement.pause = jest.fn();

		mediaPlayer.playButtonClick();
		expect(mediaPlayer.isPlaying).toBeTruthy();
		router.navigate(['norns'])
		expect(mediaPlayer.isPlaying).toBeFalsy();
	});

	it(`should switch the volume from 0% & 100% when the volume buttons are clicked`, () => {
		console.log = jest.fn();
		mediaPlayer.volumeButtonClick('down');
		expect(mediaPlayer.audioElement.volume).toEqual(0);

		mediaPlayer.volumeButtonClick('up');
		expect(mediaPlayer.audioElement.volume).toEqual(1);
	});

	it(`should change to the next track when the next button is clicked`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();

		let testTrack1 = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		let testTrack2 = new AudioFile(
			'Ninefold Museum.mp3',
			genreIdentifier,
			1
		);
		mediaPlayer.trackList = [testTrack1, testTrack2];

		expect(mediaPlayer.trackIndex).toEqual(0);
		// Click twice to assert that player stays within track boundaries
		mediaPlayer.changeTrack('next');
		mediaPlayer.changeTrack('next');
		expect(mediaPlayer.trackIndex).toEqual(1);
		mediaPlayer.changeTrack('previous');
		mediaPlayer.changeTrack('previous');
		expect(mediaPlayer.trackIndex).toEqual(0);
	});

	it(`should not change the play state as tracks are changed`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();

		let testTrack1 = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		let testTrack2 = new AudioFile(
			'Ninefold Museum.mp3',
			genreIdentifier,
			1
		);
		mediaPlayer.trackList = [testTrack1, testTrack2];

		mediaPlayer.playButtonClick();
		expect(mediaPlayer.isPlaying).toBeTruthy();
		mediaPlayer.changeTrack('next');
		expect(mediaPlayer.isPlaying).toBeTruthy();
	});

	it(`should reset the playback speed when the rate button is clicked`, () => {
		console.log = jest.fn();
		expect(1).toBe(1);
		mediaPlayer.audioElement.load = jest.fn();

		// Enable Circuit Bending
		mediaPlayer.circuitBendClick();
		fixture.detectChanges();

		mediaPlayer.rateRangeRef.nativeElement.value = "50";
		mediaPlayer.changeRate()
		expect(mediaPlayer.audioElement.playbackRate).toEqual(0.5);
		mediaPlayer.rateResetClick();
		expect(mediaPlayer.audioElement.playbackRate).toEqual(1);
	});

	it(`should reset the playback speed slider position when the rate reset button is clicked`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();

		// Enable Circuit Bending
		mediaPlayer.circuitBendClick();
		fixture.detectChanges();

		mediaPlayer.rateRangeRef.nativeElement.value = "50";
		mediaPlayer.rateResetClick();
		expect(mediaPlayer.rateRangeRef.nativeElement.value).toEqual("99");
	})

	it(`should change the media player volume when volume buttons are clicked`, () => {
		console.log = jest.fn();
		expect(mediaPlayer.audioElement.volume).toBe(1);
		mediaPlayer.volumeRangeRef.nativeElement.value = "50";
		mediaPlayer.changeVolume()
		expect(mediaPlayer.audioElement.volume).toEqual(0.5);
	});

	it(`should play the selected track when a track is clicked from the list`, async () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();

		let testTrack1 = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		let testTrack2 = new AudioFile(
			'Ninefold Museum.mp3',
			genreIdentifier,
			1
		);

		s3Service.getBucketObject = jest.fn().mockResolvedValue(Promise.resolve(testTrack2))
		mediaPlayer.trackList = [testTrack1, testTrack2];
		mediaPlayer.playTrack();
		expect(mediaPlayer.isPlaying).toBeTruthy();
		expect(mediaPlayer.trackIndex).toBe(0);

		mediaPlayer.trackClick(testTrack2);
		fixture.detectChanges();

		expect(mediaPlayer.isPlaying).toBeTruthy();
		expect(mediaPlayer.trackIndex).toBe(1);
	});

	it(`can load a track with a body`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();
		global.URL.createObjectURL = jest.fn();

		let testTrack = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		testTrack.setAudioBody(new Uint8Array());
		mediaPlayer.trackList = [testTrack];
		mediaPlayer.loadTrack(testTrack);
	});

	it(`should change the playback position as the scrub control is audio`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();

		let testTrack = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		mediaPlayer.trackList = [testTrack];
		mediaPlayer.loadTrack(testTrack);
		mediaPlayer.playTrack();

		expect(mediaPlayer.audioElement.currentTime).toBe(0);
		mediaPlayer.scrubRangeRef.nativeElement.value = "50";

		let duration = 0;
		Object.defineProperty(mediaPlayer.audioElement, 'duration', { get: () => duration });
		duration = 20;

		mediaPlayer.scrubAudio()
		expect(mediaPlayer.audioElement.currentTime).toEqual(10);
	});

	it(`should update the current time and duration label values as the track is played`, () => {
		console.log = jest.fn();
		mediaPlayer.audioElement.load = jest.fn();
		mediaPlayer.audioElement.play = jest.fn();

		let testTrack1 = new AudioFile(
			'Herselph.mp3',
			genreIdentifier,
			0
		);
		mediaPlayer.trackList = [testTrack1];

		// Manually set track duration
		let duration = 0;
		Object.defineProperty(mediaPlayer.audioElement, 'duration', { get: () => duration });
		duration = 30;

		mediaPlayer.playButtonClick();
		expect(mediaPlayer.formattedPlayTime).toBe("00:00");
		expect(mediaPlayer.formattedDuration).toBe("00:00");

		// Update Track Position
		mediaPlayer.audioElement.currentTime = 5;

		mediaPlayer.seekUpdate();
		expect(mediaPlayer.formattedPlayTime).toBe("00:05");
		expect(mediaPlayer.formattedDuration).toBe("00:25");

		duration = 140;
		mediaPlayer.audioElement.currentTime = 50;
		mediaPlayer.seekUpdate();
		expect(mediaPlayer.formattedPlayTime).toBe("00:50");
		expect(mediaPlayer.formattedDuration).toBe("01:30");

		duration = 800;
		mediaPlayer.audioElement.currentTime = 750;
		mediaPlayer.seekUpdate();
		expect(mediaPlayer.formattedPlayTime).toBe("12:30");
		expect(mediaPlayer.formattedDuration).toBe("00:50");

		mediaPlayer.audioElement.currentTime = 10;
		mediaPlayer.seekUpdate();
		expect(mediaPlayer.formattedPlayTime).toBe("00:10");
		expect(mediaPlayer.formattedDuration).toBe("13:10");

		duration = 5;
		mediaPlayer.audioElement.currentTime = 1;
		mediaPlayer.seekUpdate();
		expect(mediaPlayer.formattedPlayTime).toBe("00:01");
		expect(mediaPlayer.formattedDuration).toBe("00:04");
	});

	it(`should toggle between circuit bending mode when the bend button is clicked`, () => {
		console.log = jest.fn();
		expect(mediaPlayer.showCircuitBending).toBeFalsy();
		mediaPlayer.circuitBendClick();
		expect(mediaPlayer.showCircuitBending).toBeTruthy();
	});

	it(`can open the Archive Admin Modal`, () => {
		console.log = jest.fn();
		TestBed.createComponent(ArchiveAdminComponent);
		document.dispatchEvent(new KeyboardEvent('keydown', { 'ctrlKey': true, 'altKey': true, 'key': 'm' }));
	});

});