import { TestBed, async } from '@angular/core/testing';
import { MatListModule } from '@angular/material/list';
import { MatTabsModule } from '@angular/material/tabs';
import { NavLink } from './nav-link.model';
import { NavigatorComponent } from './navigator.component';

describe('NavigatorComponent', () => {
	beforeEach(async(() => {
		TestBed.configureTestingModule({
			imports: [
				MatTabsModule,
				MatListModule
			],
			declarations: [
				NavigatorComponent,
			],
		}).compileComponents();
	}));

	it('should create the navigator', async(() => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		expect(navigator).toBeTruthy();
	}));

	it(`should have at least one portal`, async(() => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		navigator.ngOnInit();
		expect(navigator.navigationLinks.length).toBeGreaterThan(0);
	}));

	it(`parent click should expand the portal`, async(() => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		navigator.navigationLinks = [new NavLink('Portal 1', '/portal1', true)];
		navigator.parentLinkClick(navigator.navigationLinks[0]);
		expect(navigator.navigationLinks[0].expanded).toBeTruthy();
	}));

	it(`second parent click should collapse the portal`, async(() => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		navigator.navigationLinks = [new NavLink('Portal 1', '/portal1', true)];

		// First Click [Expand]
		navigator.parentLinkClick(navigator.navigationLinks[0]);

		// Second Click [Collapse]
		navigator.parentLinkClick(navigator.navigationLinks[0]);

		expect(navigator.navigationLinks[0].expanded).toBeFalsy();
	}));

	it(`child click should select the link`, async(() => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		let parentLink = new NavLink('Portal 1', '/portal1', true);
		parentLink.children = [
			new NavLink('Child Portal', '/child', false)
		];
		navigator.navigationLinks = [parentLink];
		navigator.childLinkSelected(parentLink.children[0]);
		expect(parentLink.children[0].selected).toBeTruthy();
	}));

	it(`home click should collapse all portal`, async(() => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		navigator.navigationLinks = [new NavLink('Portal 1', '/portal1', true)];
		navigator.parentLinkClick(navigator.navigationLinks[0]);
		navigator.homeClick();
		expect(navigator.navigationLinks[0].expanded).toBeFalsy();
	}));

	it(`should determine if mobile mode on screen resize`, () => {
		const fixture = TestBed.createComponent(NavigatorComponent);
		const navigator = fixture.debugElement.componentInstance;
		navigator.onResize();
		expect(navigator.isMobile).toBeFalsy();

		global.innerWidth = 50;
		navigator.onResize();
		expect(navigator.isMobile).toBeTruthy();
	});

});