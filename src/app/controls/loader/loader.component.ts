
import { Component, OnInit } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
    selector: 'loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})

export class LoaderComponent implements OnInit {

    isMobile = false;
    isLoading: boolean = false;

    constructor(
        private loaderService: LoaderService
    ) {
    }

    ngOnInit(): void {
        this.loaderService.isLoading.subscribe((loading: boolean) => {
            this.isLoading = loading;
        });
    }
}