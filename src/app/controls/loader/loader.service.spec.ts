import { LoaderService } from "./loader.service";

describe('Loader Service Test', () => {

	it('can can start and stop the spinner from the loader service', () => {
		let loader = new LoaderService();
		loader.show();
		loader.hide();
	});
});