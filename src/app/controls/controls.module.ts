import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { FooterComponent } from './footer/footer.component';
import { MediaGroupComponent } from './media-group/media-group.component';
import { SanitizeEmbedPipe } from './media-group/sanitize-embed.pipe';
import { NavigatorComponent } from './navigator/navigator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    MediaGroupComponent,
    NavigatorComponent,
    FooterComponent,
    SanitizeEmbedPipe,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  exports: [
    NavigatorComponent,
    FooterComponent,
    MediaGroupComponent
  ]
})
export class ControlsModule { }
