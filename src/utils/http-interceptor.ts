import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { LoaderService } from 'src/app/controls/loader/loader.service';


@Injectable()
export class BusyInterceptor implements HttpInterceptor {

    private requestCount = 0;

    constructor(
        private loaderService: LoaderService
    ) {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        this.requestCount++;
        setTimeout(() => {
            this.loaderService.show();
        }, 0)
        return next.handle(request).pipe(
            finalize(() => {
                this.requestCount--;
                if (this.requestCount === 0) {
                    this.loaderService.hide();
                }
            }),
        );
    }
}