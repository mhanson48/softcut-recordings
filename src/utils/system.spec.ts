import System from "./system";

describe('System Util', () => {
	it('can determine if it is a mobile device', () => {
		global.innerWidth = 300;
		expect(System.determineMobile()).toBeTruthy();
	})

	it('can determine if it is not a mobile device', () => {
		global.innerWidth = 1000;
		expect(System.determineMobile()).toBeFalsy();
	})
});